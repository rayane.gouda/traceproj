------/*Creation du schema.*/
CREATE SCHEMA jdbc_connect;
------/*Creation de la table clients.*/
CREATE TABLE IF NOT EXISTS jdbc_connect.clients (
    id SERIAL NOT NULL PRIMARY KEY,
    nom VARCHAR NOT NULL,
    pays VARCHAR NOT NULL,
    update_ts TIMESTAMP DEFAULT current_timestamp NOT NULL
);
------/*Insertion des données dans la table clients.*/
INSERT INTO jdbc_connect.clients  (nom, pays) VALUES
('Perry Mcintosh','Poland'),
('Willow Bradley','Chile'),
('Buckminster Holt','Belgium');
------/*Trigger pour maj de la date "update_ts"*/
CREATE OR REPLACE FUNCTION maj_date()
RETURNS TRIGGER AS $$
BEGIN
    NEW.update_ts := NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER maj_date_trigger
BEFORE INSERT OR UPDATE ON jdbc_connect.clients
FOR EACH ROW
EXECUTE FUNCTION maj_date();
------/*Tres utile pour obtenir des details dans la section before de la donnée*/
ALTER TABLE jdbc_connect.clients REPLICA IDENTITY FULL;
